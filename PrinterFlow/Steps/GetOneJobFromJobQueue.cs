﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrinterFlow.Model;
using PrinterFlow.Service;
using WorkflowCore.Interface;
using WorkflowCore.Models;

namespace PrinterFlow.Steps
{
    class GetOneJobFromJobQueue : JobBaseStep
    {
        private readonly PrintQueueService printQueueService;

        public GetOneJobFromJobQueue(PrintQueueService printQueueService)
        {
            this.printQueueService = printQueueService;
        }
        public override string StepName => $"从打印队列中获取一个作业:{Job.Name}";

        public override ExecutionResult Run(IStepExecutionContext context)
        {
            while (this.printQueueService.Jobs.Count!=0)
            {
                Job = this.printQueueService.Jobs[0];
                this.printQueueService.RemoveJob(Job);
                base.Run(context);
                return ExecutionResult.Next();
            };
            throw new Exception("打印队列为空");
        }
    }
}
