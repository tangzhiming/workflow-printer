﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrinterFlow.Steps
{
    class PlatformStart : BaseStep
    {
        public override string StepName => "启动平台";
    }

    class PlatformStop : BaseStep
    {
        public override string StepName => "停止平台";
    }
}
