﻿using PrinterFlow.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkflowCore.Interface;
using WorkflowCore.Models;

namespace PrinterFlow.Steps
{
    public abstract class BaseStep : StepBody
    {
        public abstract string StepName { get; }

        public override ExecutionResult Run(IStepExecutionContext context)
        {
           ConsoleHelper.WriteLineYellow($"{context.Workflow.Id}:{StepName}");
            return ExecutionResult.Next();
        }
    }

    public abstract class WaitStep : BaseStep
    {
        public override ExecutionResult Run(IStepExecutionContext context)
        {
            ConsoleHelper.WriteLineRed($"{context.Workflow.Id}:{StepName}");
            return ExecutionResult.Next();
        }
    }
}
