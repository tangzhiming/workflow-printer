﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkflowCore.Interface;
using WorkflowCore.Models;

namespace PrinterFlow.Steps
{
    public class AddJobStep : BaseStep
    {
        public AddJobStep()
        {

        }
        public override string StepName => "添加作业到作业列表";

        public override ExecutionResult Run(IStepExecutionContext context)
        {
            return base.Run(context);
        }
    }
}
