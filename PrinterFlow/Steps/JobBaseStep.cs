﻿using PrinterFlow.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrinterFlow.Steps
{
    abstract class JobBaseStep:BaseStep
    {
        public Job  Job{ get; set; }
    }
}
