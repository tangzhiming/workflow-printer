﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkflowCore.Interface;
using WorkflowCore.Models;

namespace PrinterFlow.Steps
{
    public class InitBoardCancel : BaseStep
    {
        public override string StepName => "初始化软取消";
    }
}
