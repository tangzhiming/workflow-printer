﻿using PrinterFlow.Flow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkflowCore.Interface;
using WorkflowCore.Models;
using PrinterFlow.Context;

namespace PrinterFlow.Steps
{
    class InitWorkFlowStep : BaseStep
    {
        private readonly IWorkflowHost host;
        private readonly JobProcessContext jobProcessContext;

        public InitWorkFlowStep(IWorkflowHost  host,JobProcessContext jobProcessContext)
        {
            this.host = host;
            this.jobProcessContext = jobProcessContext;
        }
        public override string StepName => "初始化打印机工作流";
        public override ExecutionResult Run(IStepExecutionContext context)
        {
            var res = base.Run(context);
            host.StartWorkflow(nameof(JobProcessWorkFlow),1, jobProcessContext);
            host.StartWorkflow(nameof(PlatformStopWorkFlow));
            host.StartWorkflow(nameof(OpenEyeWorkFlow));
            return res;
        }
    }
}
