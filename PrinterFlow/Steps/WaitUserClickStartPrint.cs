﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkflowCore.Interface;
using WorkflowCore.Models;

namespace PrinterFlow.Steps
{
    class WaitUserClickStartPrint : WaitStep
    {
        public override string StepName => "等待用户点击打印命令";

    }
}
