﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkflowCore.Interface;
using WorkflowCore.Models;

namespace PrinterFlow.Steps
{
    class SoftwareStartup : BaseStep
    {

        public override string StepName => "软件启动";

        public override ExecutionResult Run(IStepExecutionContext context)
        {
            var res= base.Run(context);
            return res;
        }
    }
}
