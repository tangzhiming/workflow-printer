﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkflowCore.Interface;
using WorkflowCore.Models;

namespace PrinterFlow.Steps
{
    class OpenEye : BaseStep
    {
        private readonly IWorkflowHost workflowHost;

        public OpenEye(IWorkflowHost workflowHost)
        {
            this.workflowHost = workflowHost;
        }
        public override string StepName => "开电眼";

        public override ExecutionResult Run(IStepExecutionContext context)
        {
            //模拟打印完成，修改打印机状态
            Task.Delay(8000).ContinueWith(t =>
            {
                workflowHost.PublishEvent(EventName.PrintChangeToNotBusyStatus, "", DateTime.Now);
            });
            return base.Run(context);
        }
    }
    class PreTaskForOpenEye : BaseStep
    {
        public override string StepName => "开电眼前检查作业发送比例是否满足要求";
    }
}
