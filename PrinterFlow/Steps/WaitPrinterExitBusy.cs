﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrinterFlow.Steps
{
    class WaitPrinterExitBusy : WaitStep
    {
        public override string StepName => "等待打印机进入非Busy状态";
    }
}
