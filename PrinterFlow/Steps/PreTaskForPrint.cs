﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrinterFlow.Steps
{
    class PreTaskForPrint : BaseStep
    {
        public override string StepName => "打印前检查平台是否初始化，设置打印模式中的参数";
    }
}
