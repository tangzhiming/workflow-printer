﻿using PrinterFlow.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkflowCore.Interface;
using WorkflowCore.Models;

namespace PrinterFlow.Steps
{
    class AddJobToWorkQueue : BaseStep
    {
        private readonly IWorkflowHost workflowHost;
        private readonly PrintQueueService printQueueService;
        private readonly JobQueueService jobQueueService;

        public AddJobToWorkQueue(IWorkflowHost workflowHost, PrintQueueService printQueueService,JobQueueService jobQueueService)
        {
            this.workflowHost = workflowHost;
            this.printQueueService = printQueueService;
            this.jobQueueService = jobQueueService;
        }
        public override string StepName => "添加作业到打印队列";

        public override ExecutionResult Run(IStepExecutionContext context)
        {
            while (jobQueueService.Jobs.Count!=0)
            {
                var job = jobQueueService.Jobs[0];
                printQueueService.AddJob(job);
                jobQueueService.RemoveJob(job);
            }
            var res = base.Run(context);
            workflowHost.PublishEvent(EventName.PrintQueueNotEmpty, "", DateTime.Now);
            return res;

        }
    }
}
