﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkflowCore.Interface;
using WorkflowCore.Models;

namespace PrinterFlow.Steps
{
    class WaitJobQueueNotEmpty : WaitStep
    {
        public override string StepName => "等待作业队列不为空";

    }
}
