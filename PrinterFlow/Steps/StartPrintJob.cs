﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkflowCore.Interface;
using WorkflowCore.Models;

namespace PrinterFlow.Steps
{
    class StartPrintJob : JobBaseStep
    {
        public override string StepName => $"StartPrintJob:{Job.Name}";
    }
    class PrintPage : JobBaseStep
    {
        private readonly IWorkflowHost workflowHost;

        public PrintPage(IWorkflowHost workflowHost)
        {
            this.workflowHost = workflowHost;
        }
        public override string StepName => $"PrintPage:{Job.Name}";

        public override ExecutionResult Run(IStepExecutionContext context)
        {
            var res= base.Run(context);
            workflowHost.PublishEvent(EventName.NotifyOpenEye, "", DateTime.Now);
            return res;
        }
    }
    class EndJob : JobBaseStep
    {
        public override string StepName => $"EndJob:{Job.Name}";
    }

    class SetHeightFilter : JobBaseStep
    {
        public override string StepName => $"设置过滤高度:{Job.Name}";
    }
}
