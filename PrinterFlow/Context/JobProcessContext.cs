﻿using PrinterFlow.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrinterFlow.Service;

namespace PrinterFlow.Context
{
    public class JobProcessContext
    {
        public JobProcessContext()
        {

        }
        public JobProcessContext(PrintQueueService printQueueService, JobQueueService jobQueueService)
        {
            PrintQueueService = printQueueService;
            JobQueueService = jobQueueService;
        }
        public Job  Job { get; set; }

        public PrintQueueService PrintQueueService { get; set; }

        public JobQueueService JobQueueService { get; set; }
    }
}
