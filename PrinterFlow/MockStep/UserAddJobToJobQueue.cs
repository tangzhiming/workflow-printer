﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrinterFlow.Model;
using PrinterFlow.Service;
using PrinterFlow.Steps;
using WorkflowCore.Interface;
using WorkflowCore.Models;

namespace PrinterFlow.MockStep
{
    /// <summary>
    /// 用户添加作业到作业列表
    /// </summary>
    public class UserAddJobToJobQueue : BaseStep
    {
        private readonly JobQueueService jobQueueService;

        public UserAddJobToJobQueue(JobQueueService jobQueueService)
        {
            this.jobQueueService = jobQueueService;
        }
        public override string StepName => "模拟用户添加5个作业到作业列表";

        public override ExecutionResult Run(IStepExecutionContext context)
        {
            for (int i = 0; i < 5; i++)
            {
                jobQueueService.AddJob(new Job()
                {
                     Name=$"Job{i+1}"
                });
            }
            return base.Run(context);
        }
    }
}
