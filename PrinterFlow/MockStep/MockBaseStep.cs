﻿using PrinterFlow.Steps;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkflowCore.Interface;
using WorkflowCore.Models;
using ColorConsole;
using PrinterFlow.Util;

namespace PrinterFlow.MockStep
{
    public abstract class MockBaseStep : BaseStep
    {
        public override ExecutionResult Run(IStepExecutionContext context)
        {
            ConsoleHelper.WriteLineGreen(StepName);
            return ExecutionResult.Next();
        }
    }
}
