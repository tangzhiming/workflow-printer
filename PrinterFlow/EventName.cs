﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrinterFlow
{
    public class EventName
    {
        public const string UserClickPrint = nameof(UserClickPrint);

        public const string PrintChangeToNotBusyStatus = nameof(PrintChangeToNotBusyStatus);

        public const string NotifyOpenEye = nameof(NotifyOpenEye);

        public const string PrintQueueNotEmpty = nameof(PrintQueueNotEmpty);

        public const string SoftwareStartupFinished = nameof(SoftwareStartupFinished);
    }
}
