﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkflowCore.Interface;

namespace PrinterFlow
{
   public  class PrinterContext
    {
        IWorkflowHost Host { get; set; }

        PrinterStatus Status { get; set; }
    }

    public enum PrinterStatus
    {
        Ready,
        Busy
    }
}
