﻿using PrinterFlow.Steps;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkflowCore.Interface;

namespace PrinterFlow.Flow
{
    class PlatformStopWorkFlow : IWorkflow
    {
        public string Id => nameof(PlatformStopWorkFlow);

        public int Version => 1;

        public void Build(IWorkflowBuilder<object> builder)
        {
            builder.StartWith<WaitPrinterExitBusy>()
                .WaitFor(EventName.PrintChangeToNotBusyStatus,data=>"", o => DateTime.Now)
                    .Then<PlatformStop>();
        }
    }
}
