﻿using PrinterFlow.Context;
using PrinterFlow.Model;
using PrinterFlow.Steps;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkflowCore.Interface;

namespace PrinterFlow.Flow
{
    class JobProcessWorkFlow : IWorkflow<JobProcessContext>
    {
        public string Id => nameof(JobProcessWorkFlow);

        public int Version => 1;

        public void Build(IWorkflowBuilder<object> builder)
        {
            builder.StartWith<DoNothingStep>().While(c => true).Do(b =>
                b.StartWith<WaitJobQueueNotEmpty>()
                .WaitFor(EventName.PrintQueueNotEmpty, data => "", o => DateTime.Now)
                .Then<GetOneJobFromJobQueue>()
                .Then<StartPrintJob>()
                .Then<PrintPage>()
                .Then<SetHeightFilter>());

        }

        public void Build(IWorkflowBuilder<JobProcessContext> builder)
        {
            builder.StartWith<DoNothingStep>().While(c => true).Do(b =>
              b.StartWith<WaitJobQueueNotEmpty>()
              .WaitFor(EventName.PrintQueueNotEmpty, data => "", o => DateTime.Now)

              .While(c => c.PrintQueueService.Jobs.Count != 0).Do(w => w.StartWith<DoNothingStep>()
                .Then<GetOneJobFromJobQueue>()
                .Output(data => data.Job, step => step.Job)
                .Then<StartPrintJob>()
                .Input(step => step.Job, v => v.Job)
                .Then<PrintPage>()
                .Input(step => step.Job, v => v.Job)
                .Then<SetHeightFilter>()
                .Input(step => step.Job, v => v.Job))
              );
        }
    }
}
