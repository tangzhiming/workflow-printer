﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkflowCore.Interface;
using PrinterFlow.Steps;
using WorkflowCore.Models;
using PrinterFlow.MockStep;

namespace PrinterFlow.Flow
{
    class StartPrintWorkFlow : IWorkflow
    {

        public string Id => nameof(StartPrintWorkFlow);

        public int Version => 1;



        public void Build(IWorkflowBuilder<object> builder)
        {
            builder.StartWith<SoftwareStartup>()
                .Then<InitWorkFlowStep>()
                .While(c => true).
                 Do(b =>
                 b.StartWith<WaitUserClickStartPrint>()
                 .WaitFor(nameof(WaitUserClickStartPrint), data => "",o=>DateTime.Now)
                .Then<InitBoardCancel>()
                .Then<UserAddJobToJobQueue>()
                .Then<PreTaskForPrint>()
                .Then<AddJobToWorkQueue>())
                ;
        }
    }
}
