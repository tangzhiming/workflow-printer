﻿using PrinterFlow.Steps;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkflowCore.Interface;

namespace PrinterFlow.Flow
{
    class OpenEyeWorkFlow : IWorkflow
    {
        public string Id => nameof(OpenEyeWorkFlow);

        public int Version => 1;

        public void Build(IWorkflowBuilder<object> builder)
        {
            builder.StartWith<DoNothingStep>()
                .While(c => true).Do(b => b.StartWith<WaitNotifyOpenEye>()
                   .WaitFor(EventName.NotifyOpenEye, data => "", o => DateTime.Now)
                 .Then<PreTaskForOpenEye>()
                 .Then<PlatformStart>()
                 .Then<OpenEye>());

        }
    }
}
