﻿using PrinterFlow.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrinterFlow.Service
{
    /// <summary>
    /// 作业队列
    /// </summary>
    public class JobQueueService
    {
        public List<Job> Jobs { get; set; } = new List<Job>();
        public void AddJob(Job job)
        {
            this.Jobs.Add(job);
        }

        public void RemoveJob(Job job)
        {
            this.Jobs.Remove(job);
        }
    }
}
