﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ColorConsole;

namespace PrinterFlow.Util
{
    public class ConsoleHelper
    {
        public static ConsoleWriter Writer = new ConsoleWriter();

        public static void WriteLine<T>(T msg,ConsoleColor color)
        {
            Writer.WriteLine(msg, color);
        }

        public static void WriteLineGreen<T>(T msg)
        {
            Writer.WriteLine(msg, ConsoleColor.Green);
        }

        public static void WriteLineYellow<T>(T msg)
        {
            Writer.WriteLine(msg, ConsoleColor.Yellow);
        }

        public static void WriteLineRed<T>(T msg)
        {
            Writer.WriteLine(msg, ConsoleColor.Red);
        }
    }
}
