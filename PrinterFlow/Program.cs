﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkflowCore.Interface;
using PrinterFlow.Flow;
using PrinterFlow.Steps;
using System.Threading;
using WorkflowCore.Models;
using PrinterFlow.Service;
using PrinterFlow.Context;
using PrinterFlow.MockStep;
using Microsoft.Extensions.Logging;

namespace PrinterFlow
{
    class Program
    {
        static void Main(string[] args)
        {
            IServiceProvider serviceProvider = ConfigureServices();
            var host = serviceProvider.GetService<IWorkflowHost>();
            host.RegisterWorkflow<StartPrintWorkFlow>();
            host.RegisterWorkflow<JobProcessWorkFlow,JobProcessContext>();
            host.RegisterWorkflow<PlatformStopWorkFlow>();
            host.RegisterWorkflow<OpenEyeWorkFlow>();


            host.Start();

            host.StartWorkflow(nameof(StartPrintWorkFlow));

            Console.WriteLine("点击回车键，模拟用户点击打印按钮");

            while (true)
            {
                Console.ReadLine();
                Console.WriteLine("用户点击了打印");
                host.PublishEvent(nameof(WaitUserClickStartPrint), "", null);
            }


            Console.ReadLine();
            host.Stop();
        }

        private static IServiceProvider ConfigureServices()
        {
            //setup dependency injection
            IServiceCollection services = new ServiceCollection();
            services.AddLogging();
            services.AddWorkflow();
            //services.AddWorkflow(x => x.UseMongoDB(@"mongodb://localhost:27017", "workflow"));
            
            services.AddTransient<AddJobToWorkQueue>();
           
            services.AddTransient<AddJobToWorkQueue>();
            services.AddTransient<InitWorkFlowStep>();
            services.AddTransient<OpenEye>();
            services.AddTransient<PrintPage>();
            services.AddTransient<UserAddJobToJobQueue>();
            services.AddTransient<GetOneJobFromJobQueue>();

            services.AddSingleton<JobQueueService>();
            services.AddSingleton<PrintQueueService>();
            services.AddSingleton<JobProcessContext>();

            var factory= LoggerFactory.Create(b => b.AddConsole());
            services.AddSingleton(factory);
             var serviceProvider = services.BuildServiceProvider();
         
            return serviceProvider;
        }
    }
}
